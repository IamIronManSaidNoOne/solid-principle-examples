class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None


def circular_linked_list(head):
    if head is None:
        return True

    node = head.next
    i = 0

    while (node is not None) and (node is not head):
        i = i + 1
        node = node.next

    return node is head

if __name__ == "__main__":
    linked_list = LinkedList()
    linked_list.head = Node(1)
    second = Node(2)
    third = Node(3)
    fourth = Node(4)

    linked_list.head.next = second
    second.next = third
    third.next = fourth

    if circular_linked_list(linked_list.head):
        print('yes')
    else:
        print('no')

    fourth.next = linked_list.head

    if circular_linked_list(linked_list.head):
        print('yes')
    else:
        print('no')
