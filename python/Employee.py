#Py OOPS
class Employee:

    num_of_emps = 0
    raise_amount = None

    def __init__(self, first, last, pay):
        self.first = first
        self.last = last
        self.pay = pay
        self.email = first + '.' + last + '@company.com'

        Employee.num_of_emps += 1

    def fullname(self):
        return '{} {}'.format(self.first, self.last)

    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amount)

    @classmethod
    def set_raise_amt(cls, amount):
        cls.raise_amount = amount

emp_1 = Employee('Nick', 'Nalli', 60000)
emp_2 = Employee('Test', 'Subject', 65000)

print(emp_1.fullname())
print(emp_2.fullname())

Employee.set_raise_amt(0.5)

print(emp_1.pay)
emp_1.apply_raise()
print(emp_1.pay)

print(Employee.__dict__)