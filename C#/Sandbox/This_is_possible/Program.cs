﻿using System;

namespace This_is_possible
{
    class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Choose a number to use program: \n1.Random Phone Number \n2.TupleValues \n3.Scrap Pad Program \n4.RepeatingFraction");
            var inputAnswer = Convert.ToInt32(Console.ReadLine());
            if (inputAnswer == 1)
            {
                var randomPhoneNumber = RandomGenerator.RandomGenerator.PhoneNumber();
                Console.WriteLine($"The random Number: {randomPhoneNumber}");
                var firstName = RandomGenerator.RandomGenerator.FirstName(10);
                var lastName = RandomGenerator.RandomGenerator.LastName(10);
                var middleName = RandomGenerator.RandomGenerator.MiddleName(8);
                var streetName = RandomGenerator.RandomGenerator.StreetName(15);
                var streetName2 = RandomGenerator.RandomGenerator.Address2(25);
                Console.WriteLine($"The first name: {firstName}");
                Console.WriteLine($"The middle name: {middleName}");
                Console.WriteLine($"The last name: {lastName}");
                Console.WriteLine($"Will the real {firstName} {middleName} {lastName}, please stand up!");
                Console.WriteLine($"The Address is: {streetName}\n{streetName2}");
                Console.ReadKey(); 
            }
            if (inputAnswer == 3)
            {
                var result = ScrapPad.FindMedianSortedArrays();
                Console.WriteLine($"result: {result}");
            }
            if (inputAnswer == 4)
            {
                var result = ScrapPad.FractionToDecimal(2, 333);
                Console.WriteLine($"result: {result}");
            }
            else
            {
                var info = new TupleList<string, string, int>
                {
                    {"John Doe", "john.doe@gmail.com", 1234567890 },
                    {"Jane Doe", "jane.doe@gmail.com", 0123456789 },
                    {"Grace Van pelt", "agent.pelt@cbi.com", 985678210 },
                };

                var info2 = new TupleList<int, int, string>
                {
                    {1,2,"Mic Drop" }
                };
                
                Console.WriteLine($"The first info in the tuple list is \nName: {info[0].Item1} \nE-mail: {info[0].Item2} \nContact: {info[0].Item3}");
                Console.WriteLine($"\n\nThe second info in the tuple list is \nName: {info[1].Item1} \nE-mail: {info[1].Item2} \nContact: {info[1].Item3}");
                Console.WriteLine($"\n\nThe third info in the tuple list is \nName: {info[2].Item1} \nE-mail: {info[2].Item2} \nContact: {info[2].Item3}");
                Console.WriteLine($"\n\nThe third info in the tuple list is \nName: { info[2].Item1} \nE-mail: { info[2].Item2} \nContact: { info[2].Item3}");
                Console.WriteLine($"\n\nThe third info in the tuple list is \nint: {info2[0].Item1} \nsecond int: {info2[0].Item2} \npunchline: {info2[0].Item3}");
                Console.ReadKey();
            }
        }
    }
}
