﻿using System;
using System.Collections.Generic;

namespace This_is_possible
{
    public class TupleList<T1, T2, T3> : List<Tuple<T1, T2, T3>>
    {
        public void Add(T1 item1, T2 items2, T3 item3)
        {
            Add(new Tuple<T1, T2, T3>(item1, items2, item3));
        }
    }
}
