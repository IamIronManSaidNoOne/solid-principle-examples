﻿using System.Collections.Generic;

namespace This_is_possible
{
    class ScrapPad
    {
        public static double FindMedianSortedArrays()
        {
            int[] nums1 = { 1, 2 };
            int[] nums2 = { 3, 4 };
            List<int> summedArrays = new List<int>();
            foreach (var item in nums1)
            {
                summedArrays.Add(item);
            }
            foreach (var item in nums2)
            {
                summedArrays.Add(item);
            }
            summedArrays.Sort();
            var arrayCount = summedArrays.Count;
            if (arrayCount % 2 != 0)
            {
                var resultPosition = arrayCount / 2;
                return _ = summedArrays[resultPosition];
            }
            var tempArrayCount = arrayCount / 2;
            double tempMedian1 = summedArrays[tempArrayCount];
            double tempMedian2 = summedArrays[tempArrayCount - 1];
            return _ = (tempMedian1 + tempMedian2) / 2;
        }

        public static string FractionToDecimal(int numerator, int denominator)
        {
            if (numerator == 0)
            {
                return "0";
            }
            if (denominator == 0)
            {
                return string.Empty;
            }
            double remainderResult = numerator % denominator;
            decimal quotientResult = numerator / denominator;
            if (remainderResult != 0.0)
            {
                for (double i = remainderResult; i <= remainderResult; i++)
                {
                    var tmpRResult = remainderResult % denominator;
                    var tmpQResult = remainderResult / denominator;
                    if (tmpRResult != 0)
                    {
                        var result = $"{tmpQResult}";
                        if (result.Contains(".") && result.Length > 4)
                        {
                            var nonRepeatChar = result.Substring(0, 1);
                            var repeatCharsArray = result.Substring(2, result.Length - 2).ToCharArray();
                            var repeatChar = string.Empty;
                            for (int j = 0; j < repeatCharsArray.Length; j++)
                            {
                                if (!repeatChar.Contains(repeatCharsArray[j]) && repeatChar != "0")
                                {
                                    var strChar = repeatCharsArray[j].ToString();
                                    repeatChar += strChar;
                                }
                            }
                            result = $"{nonRepeatChar}.({repeatChar})";
                        }
                        return result;
                    }
                    break;
                }
            }
            return quotientResult.ToString();
        }
    }
}
