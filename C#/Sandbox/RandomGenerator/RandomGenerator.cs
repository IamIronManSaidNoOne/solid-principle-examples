﻿using System;
using System.Collections.Generic;

namespace RandomGenerator
{
    public class RandomGenerator
    {
        public static List<int> RandomizedInt(int min, int max)
        {
            var result = new List<int>();
            Random random = new Random();
            for (int i = min; i < max; i++)
            {
                var r = random.Next(min, max);
                result.Add(r);
            }
            return result; 
        }
        public static string PhoneNumber()
        {
            var resultRandom = new List<int>();
            Random random = new Random();
            for (int i = 0; i < 9; i++)
            {
                var r = random.Next(0, 9);
                if (r != 0)
                {
                    resultRandom.Add(r);
                }
                else
                {
                    i++;
                }
            }
            var result = string.Join("", resultRandom.ToArray());
            return result; 
        }
        public static string FirstName(int length)
        {
            return RandomAlphabets(length); 
        }
        public static string LastName(int length)
        {
            return RandomAlphabets(length); 
        }
        public static string MiddleName(int length)
        {
            return RandomAlphabets(length); 
        }
        public static string StreetName(int length)
        {
            return RandomAlphabets(length); 
        }
        public static string Address2(int length)
        {
            return RandomString(length); 
        }

        private static string RandomAlphabets(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var stringChars = new char[length];
            var random = new Random();
            for (int i = 0; i < length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            var result = new string(stringChars);
            return result;
        }

        private static string RandomString(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,.-";
            var stringChars = new char[length];
            var random = new Random();
            for (int i = 0; i < length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            var result = new string(stringChars);
            return result;
        }
    }
}
