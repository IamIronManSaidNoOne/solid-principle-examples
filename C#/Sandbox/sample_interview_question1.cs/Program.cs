﻿using System;

namespace sample_interview_question1.cs
{
    class Program
    {
        static bool isPresent(char []arr1, char []arr2, int m, int n)
        {
            int i = 0;
            int j = 0;
            for (i = 0; i < n; i++)
            {
                for (j = 0;  j < m; j++)
                {
                    if (arr1[i] == arr2[j])
                    {
                        break;
                    }
                }

                if (j == m)
                {
                    return false;
                }
            }
            return true;
        }

        static char[] Split(string given)
        {
            char[] letter = given.ToCharArray();
            return letter;
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("\n Select one of the following by entering the number:");
            Console.WriteLine("\n 1. Binary Tree Search \n 2. Hash code");
            int option = Convert.ToInt16(Console.ReadLine());

            if (option == 1)
            {
                string a = "aaaaabcjdefkabcllseo";
                string b = "abc";

                char[] arr1 = Split(a);
                char[] arr2 = Split(b);

                int m = arr1.Length;
                int n = arr2.Length;

                if (isPresent(arr1, arr2, m, n))
                {
                    Console.WriteLine("b has the characters of a");
                    Console.ReadKey();
                }
                else
                {
                    Console.WriteLine("b doesn't have the characters of a");
                    Console.ReadKey();
                }
            }

            if (option == 2)
            {
                string[] givenKeys = new string[]
                {
                    "Nick",
                    "Tick",
                    "Click",
                    "Flick",
                    "Chick",
                    "Rick"
                };
                SampleQuestion2.HashSortString(givenKeys);
                Console.ReadKey();
            }


        }
    }
}
