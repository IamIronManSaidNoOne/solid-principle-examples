﻿namespace sample_interview_question1.cs
{
    public class Node
    {
        public Node Root { get; set; }
        public Node Left { get; set; }
        public Node Right { get; set; }
    }
}
