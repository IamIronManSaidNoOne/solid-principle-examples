﻿/*Write code in C# to Hash an array of keys and display them with their hash code.*/
using System;

namespace sample_interview_question1.cs
{
    public class SampleQuestion2
    {
        static int HasFunction(string s, string[] array)
        {
            int total = 0;
            char[] c;
            c = s.ToCharArray();
            for (int i = 0; i <= c.GetUpperBound(0); i++)
            {
                total += (int) c[i];
            }

            return total % array.GetUpperBound(0);
        }
        public static void HashSortString(string[] strings)
        {
            string[] values = new string[50];
            string str;

            string[] keys = new string[]
            {
                "Nick",
                "Tick",
                "Click",
                "Flick",
                "Chick",
                "Rick",
                "Sick",
                "Mick",
                "Sleek"
            };

            int hashCode;

            for (int i = 0; i < 9; i++)
            {
                str = keys[i];
                hashCode = HasFunction(str, values);

                values[hashCode] = str;
            }

            for (int k = 0; k < values.GetUpperBound(0); k++)
            {
                if (values[k] != null)
                {
                    Console.WriteLine(k + " " + values[k]);
                    Console.ReadKey();
                }
            }
        }
    }
}