﻿using System;

namespace recursion_sample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please Enter a numbet that you would like to see the factorial :");
            ulong reqNum = Convert.ToUInt64(Console.ReadLine());
            ulong resultNum = Factorial(reqNum);
            Console.WriteLine("\nAnswer is: {0}", resultNum);
            Console.ReadKey();
        }
        private static ulong Factorial(ulong val)
        {
            //Exit condition5
            if (val == 0)
            {
                return 1;
            }
            return val *= Factorial(val-1);
        }
    }
}
