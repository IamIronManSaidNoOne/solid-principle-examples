﻿using System;

namespace bigO
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the number between 0 to 10");
            var requiredNum = Convert.ToInt32(Console.ReadLine());
            int[] array = new[] {1, 2, 3, 4, 5, 6, 7, 8, 9};
            var result = LogTimeComplexity.DoSearch(array, requiredNum);
            if (result ==  -1)
            {
                Console.WriteLine("\nThat isn't the number you are looking for");
            }
            else
            {
                Console.WriteLine("\nThe position of the number {0} is {1}", requiredNum, result);
            }
            Console.ReadKey();
        }
    }
}
