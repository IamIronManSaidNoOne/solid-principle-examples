namespace bigO
{
    public class LogTimeComplexity
    {
        public static int DoSearch(int[] array, int targetValue)
        {
            var minIndex = 0;
            var maxIndex = array.Length - 1;
            var currentIndex = 0;
            var currentElement = 0;
            while(minIndex <= maxIndex)
            {
                currentIndex = (minIndex + maxIndex)/2 | 0;
                currentElement = array[currentIndex];
                if (currentElement < targetValue)
                {
                    minIndex = currentIndex + 1;
                }
                else if (currentElement > targetValue)
                {
                    maxIndex = currentIndex - 1;
                }
                else
                {
                    return currentIndex;
                }

            }

            return -1;

        }
    }
}