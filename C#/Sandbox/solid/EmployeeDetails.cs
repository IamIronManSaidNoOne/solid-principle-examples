using System;
namespace solid {
    public class EmployeeDetails {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string eMail { get; set; }
        public Int32 phoneNumber { get; set; }

        public void EmployeeRegistration (EmployeeDetails employee) {
            StaffData.Employees.Add (employee);
        }

    }
}