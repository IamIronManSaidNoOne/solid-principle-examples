using System;

namespace solid {
    class Program {

        static void Main (string[] args) {
            EmployeeDetails employeeDetails = new EmployeeDetails {
                firstName = "Nick",
                lastName = "Nalli",
                eMail = "nick.nalli@gmail.com",
                phoneNumber = unchecked ((int) 4021234567),

            };

            employeeDetails.EmployeeRegistration (employeeDetails);
            Console.ReadKey ();
        }
    }
}