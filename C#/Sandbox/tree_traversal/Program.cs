﻿using System;

namespace tree_traversal
{
    class Program
    {
        private static void Main(string[] args)
        {
            BinaryTree bST = new BinaryTree();
            bST.Insert(30);
            bST.Insert(35);
            bST.Insert(57);
            bST.Insert(15);
            bST.Insert(63);
            bST.Insert(49);
            bST.Insert(89);
            bST.Insert(77);
            bST.Insert(67);
            bST.Insert(98);
            bST.Insert(91);
            Console.WriteLine("\nPreOrder Traversal : ");
            bST.PreOrder(bST.ReturnRoot());
            Console.WriteLine("\n");
            Console.ReadKey();
            Console.WriteLine("\nInOrder Traversal : ");
            bST.InOrder(bST.ReturnRoot());
            Console.WriteLine("\n");
            Console.ReadKey();
            Console.WriteLine("\nPostOrder Traversal : ");
            bST.PostOrder(bST.ReturnRoot());
            Console.WriteLine("\n");
            Console.ReadKey();
        }
    }
}
