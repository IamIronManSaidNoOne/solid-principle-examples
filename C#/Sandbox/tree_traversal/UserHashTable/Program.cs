﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Diagnostics;

//namespace UserHashTable
//{
//    struct UserInfo
//    {
//        public int userId;
//        public string userName;

//        public UserInfo(int id, string name)
//        {
//            userId = id;
//            userName = name;
//        }
//    }
    
//    class Program
//    {
//        private static Hashtable userInfoHash;
//        private static List<UserInfo> userInfoList;
//        private static Stopwatch stopwatch;
//        public static void Main(string[] args)
//        {
//            userInfoHash = new Hashtable();
//            userInfoList = new List<UserInfo>();
//            stopwatch = new Stopwatch();
//            //Adding users
//            for (int i = 0; i < 4000000; i++)
//            {
//               userInfoHash.Add(i, "user" + i);
//                userInfoList.Add(new UserInfo(i, "user" + i));
//            }

//            //Removing users
//            if (userInfoHash.ContainsKey(0))
//            {
//                userInfoHash.Remove(0);
//            }

//            //Setting
//            if (userInfoHash.ContainsKey(1))
//            {
//                userInfoHash[1] = "replacementName";
//            }

//            //Looping
//            //foreach (DictionaryEntry entry in userInfoHash)
//            //{
//            //    Console.WriteLine("Key: "+ entry.Key + "/ Value: " + entry.Value);
//            //}

//            //Access
//            Random randomUserGen = new Random();
//            int randomUser = -1;

//            stopwatch.Start();
//            float startTime = 0;
//            float endTime = 0;
//            float deltaTime = 0;

//            int cycles = 5;
//            int cycle = 0;
//            string userName = string.Empty;
//            while (cycle < cycles)
//            {
//                randomUser = randomUserGen.Next(3000000, 4000000);

//                startTime = stopwatch.ElapsedMilliseconds;
//                //Access logic for the list
//                userName = GetUserFromList(randomUser);

//                endTime = stopwatch.ElapsedMilliseconds;
//                deltaTime = endTime - startTime;
//                Console.WriteLine("Time taken to retrieve " + userName + " from list took " + string.Format("{0:0.##}", deltaTime) + "ms");

//                startTime = stopwatch.ElapsedMilliseconds;
//                //Access logic for the hash
//                userName = (string) userInfoHash[randomUser];
//                endTime = stopwatch.ElapsedMilliseconds;
//                deltaTime = endTime - startTime;
//                Console.WriteLine("Time taken to retrieve " + userName + " from hash took " + string.Format("{0:0.##}", deltaTime) + "ms");
//                Console.WriteLine("\n");

//                cycle++;
//            }


//            Console.ReadKey();
//        }
//        static string GetUserFromList(int userId)
//        {
//            for (int i = 0; i < userInfoList.Count; i++)
//            {
//                if (userInfoList[i].userId == userId)
//                {
//                    return userInfoList[i].userName;
//                }
//            }

//            return string.Empty;
//        }
//    }
//}

