﻿using System;

namespace tree_traversal
{
    public class BinaryTree
    {
        public Node Root { get; set; }

        public Node ReturnRoot()
        {
            return Root;
        }

        public void Insert(int id)
        {
            Node newNode = new Node();
            newNode.Data = id;
            if (Root == null)
            {
                Root = newNode;
            }
            else
            {
                Node current = Root;
                Node parent;
                while (true)
                {
                    parent = current;
                    if (id < current.Data)
                    {
                        current = current.LeftNode;
                        if (current == null)
                        {
                            parent.LeftNode = newNode;
                            return;
                        }
                    }
                    else
                    {
                        current = current.RightNode;
                        if (current == null)
                        {
                            parent.RightNode = newNode;
                            return;
                        }
                    }
                }
            }
        }

        public void PreOrder(Node Root)
        {
            if (Root != null)
            {
                Console.Write(Root.Data + " ");
                PreOrder(Root.LeftNode);
                PreOrder(Root.RightNode);
            }
        }

        public void InOrder(Node Root)
        {
            if(Root != null)
            {
                InOrder(Root.LeftNode);
                Console.Write(Root.Data + " ");
                InOrder(Root.RightNode);
            }
        }

        public void PostOrder(Node Root)
        {
            if (Root != null)
            {
                PostOrder(Root.LeftNode);
                PostOrder(Root.RightNode);
                Console.Write(Root.Data + " ");
            }
        }
    }
}